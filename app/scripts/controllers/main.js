'use strict';

/**
 * @ngdoc function
 * @name imagierApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the imagierApp
 */
angular.module('imagierApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
