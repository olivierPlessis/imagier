'use strict';

/**
 * @ngdoc function
 * @name imagierApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the imagierApp
 */
angular.module('imagierApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
